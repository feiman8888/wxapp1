# wxapp1

#### 介绍
Bidcms开源分销商城 微信小程序单店版

#### 软件架构
软件架构说明


#### 安装教程
此小程序基于
https://github.com/bidcms/wechat-app-mall
修改，bidcms兼容所有功能，此版本不带分销功能

ext.json为第三方平台所需参数，如果独产开发请删除此文件
示例中appid,商城id,版本号均从此文件中取得，如果不使用可以修改utils/util.js中request方法，将appid，商城id固定
登录 http://shop.bidcms.com 开通商城，并上传商品

#### 使用说明

1. 登录http://shop.bidcms.com
   开通商城功能，如果自己搭建需要定制开发,联系qq:2559009123 微信：bidcms
2. 添加商品和完善商城信息
3. 发布小程序

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)