var wxpay = require('../../../utils/pay.js')
var app = getApp()
var api = require("../../../constant/api.js");
var util = require("../../../utils/util.js");
Page({
  data: {
    tabs: [{ "id": 0, "css": "", "name": "\u5168\u90e8", "params": "status=all&type=" }, { "id": 0, "css": "", "name": "\u672a\u652f\u4ed8", "params": "status=0&type=" }, { "id": 1, "css": "", "name": "\u5df2\u4ed8\u6b3e", "params": "status=1&type=" }, { "id": 2, "css": "", "name": "\u5df2\u53d1\u8d27", "params": "status=2&type=" }],
    stv: {
      windowWidth: 0,
      lineWidth: 0,
      offset: 0,
      tStart: false
    },
    activeTab: 0,
    loadingStatus: false,
  },
  onLoad: function (options) {
    try {
      let { tabs } = this.data;
      var res = wx.getSystemInfoSync()
      this.windowWidth = res.windowWidth;
      this.data.stv.lineWidth = this.windowWidth / this.data.tabs.length;
      this.data.stv.windowWidth = res.windowWidth;
      this.setData({ stv: this.data.stv })
      this.tabsCount = tabs.length;
    } catch (e) {
    }
  },
  onShow: function () {
    // 获取订单列表
    this.setData({
      loadingStatus: true
    })
    this.getOrderStatistics();
    this.getOrderList()
  },
  getOrderStatistics: function () {
    var that = this;
    util.request({
      url: '/order/statistics',
      data: {},
      success: (res) => {
        wx.hideLoading();
        if (res.data.code == 0) {
          var tabClass = that.data.tabClass;
          if (res.data.data.count_id_no_pay > 0) {
            tabClass[0] = "red-dot"
          } else {
            tabClass[0] = ""
          }
          if (res.data.data.count_id_no_transfer > 0) {
            tabClass[1] = "red-dot"
          } else {
            tabClass[1] = ""
          }
          if (res.data.data.count_id_no_confirm > 0) {
            tabClass[2] = "red-dot"
          } else {
            tabClass[2] = ""
          }
          if (res.data.data.count_id_no_reputation > 0) {
            tabClass[3] = "red-dot"
          } else {
            tabClass[3] = ""
          }
          if (res.data.data.count_id_success > 0) {
            //tabClass[4] = "red-dot"
          } else {
            //tabClass[4] = ""
          }

          console.log(tabClass)
          that.setData({
            tabClass: tabClass,
          });
        }
      }
    })
  },
  getOrderList: function () {
    var that = this;
    var postData = {
      token: wx.getStorageSync('token'),
      pageSize: app.globalData.pageSize,
      page: app.globalData.page
    };
    console.log('getting orderList')
    util.request({
      url: '/order/list',
      data: postData,
      success: (res) => {
        if (res.data.code === 0) {
          that.setData({
            tabs: res.data.data.tabs,
            totalOrderList: res.data.data.orderList,
            goodsMap: res.data.data.orderGoods,
            tabsCount: res.data.data.tabs.length
          });
          //订单分类
          var orderList = [];
          for (let i in that.data.tabs) {
            var tempList = [];
            for (let j = 0; j < res.data.data.orderList.length; j++) {
              if (res.data.data.orderList[j].order_status == that.data.tabs[i].id) {
                tempList.push(res.data.data.orderList[j])
                //orderList[i].push(res.data.data.orderList[j])
              }
            }
            orderList.push({ 'status': i, 'isnull': tempList.length === 0, 'orderList': tempList })
          }
          this.setData({
            orderList: orderList
          });
        } else {
          that.setData({
            orderList: 'null',
            logisticsMap: {},
            goodsMap: {}
          });
        }
        this.setData({
          loadingStatus: false
        })
      },
      fail: (res) =>{
        console.log('获取orderList错误',res.data)
      }
    })
  },
  orderDetail: function (e) {
    var orderId = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: "/pages/order-details/index?id=" + orderId
    })
  },
  cancelOrderTap: function (e) {
    var that = this;
    var orderId = e.currentTarget.dataset.id;
    wx.showModal({
      title: '确定要取消该订单吗？',
      content: '',
      success: function (res) {
        if (res.confirm) {
          wx.showLoading();
          util.request({
            url: '/order/close',
            data: {
              orderId: orderId
            },
            success: (res) => {
              wx.hideLoading();
              if (res.data.code == 0) {
                that.onShow();
              }
            }
          },-1,null,["order/list","orderDetail"+orderId])
        }
      }
    })
  },
  toPayTap: function (e) {
    var that = this;
    var orderId = e.currentTarget.dataset.id;
    let remark = "在线充值";
    let nextAction = {};
    if (orderId != 0) {
      remark = "支付订单 ：" + orderId;
      nextAction = { type: 0, id: orderId };
    }
    util.request({
      url: '/order/pay',
      data: {
        id: orderId
      },
      //method:'POST',
      success: function (res) {
        if (res.data.code == 0) {
          // 发起支付
          wx.requestPayment({
            timeStamp: res.data.data.timeStamp,
            nonceStr: res.data.data.nonceStr,
            package: res.data.data.package,
            signType: 'MD5',
            paySign: res.data.data.paySign,
            fail: function (aaa) {
              wx.showToast({ title: '支付失败:' + aaa })
            },
            success: function () {
              wx.showToast({ title: '支付成功' })
              wx.redirectTo({
                url: redirectUrl
              });
            }
          })
        } else {
          wx.showToast({ title: '服务器忙' + res.data.code + res.data.msg })
        }
      }
    },-1)
  },
  ////////
  handlerStart(e) {
    console.log('handlerStart')
    let { clientX, clientY } = e.touches[0];
    this.startX = clientX;
    this.tapStartX = clientX;
    this.tapStartY = clientY;
    this.data.stv.tStart = true;
    this.tapStartTime = e.timeStamp;
    this.setData({ stv: this.data.stv })
  },
  handlerMove(e) {
    console.log('handlerMove')
    let { clientX, clientY } = e.touches[0];
    let { stv } = this.data;
    let offsetX = this.startX - clientX;
    this.startX = clientX;
    stv.offset += offsetX;
    if (stv.offset <= 0) {
      stv.offset = 0;
    } else if (stv.offset >= stv.windowWidth * (this.tabsCount - 1)) {
      stv.offset = stv.windowWidth * (this.tabsCount - 1);
    }
    this.setData({ stv: stv });
  },
  handlerCancel(e) {

  },
  handlerEnd(e) {
    console.log('handlerEnd')
    let { clientX, clientY } = e.changedTouches[0];
    let endTime = e.timeStamp;
    let { tabs, stv, activeTab } = this.data;
    let { offset, windowWidth } = stv;
    //快速滑动
    if (endTime - this.tapStartTime <= 300) {
      console.log('快速滑动')
      //判断是否左右滑动(竖直方向滑动小于50)
      if (Math.abs(this.tapStartY - clientY) < 50) {
        //Y距离小于50 所以用户是左右滑动
        console.log('竖直滑动距离小于50')
        if (this.tapStartX - clientX > 5) {
          //向左滑动超过5个单位，activeTab增加
          console.log('向左滑动')
          if (activeTab < this.tabsCount - 1) {
            this.setData({ activeTab: ++activeTab })
          }
        } else if (clientX - this.tapStartX > 5) {
          //向右滑动超过5个单位，activeTab减少
          console.log('向右滑动')
          if (activeTab > 0) {
            this.setData({ activeTab: --activeTab })
          }
        }
        stv.offset = stv.windowWidth * activeTab;
      } else {
        //Y距离大于50 所以用户是上下滑动
        console.log('竖直滑动距离大于50')
        let page = Math.round(offset / windowWidth);
        if (activeTab != page) {
          this.setData({ activeTab: page })
        }
        stv.offset = stv.windowWidth * page;
      }
    } else {
      let page = Math.round(offset / windowWidth);
      if (activeTab != page) {
        this.setData({ activeTab: page })
      }
      stv.offset = stv.windowWidth * page;
    }
    stv.tStart = false;
    this.setData({ stv: this.data.stv })
  },
  ////////
  _updateSelectedPage(page) {
    console.log('_updateSelectedPage')
    let { tabs, stv, activeTab } = this.data;
    activeTab = page;
    this.setData({ activeTab: activeTab })
    stv.offset = stv.windowWidth * activeTab;
    this.setData({ stv: this.data.stv })
  },
  handlerTabTap(e) {
    console.log('handlerTapTap', e.currentTarget.dataset.index)
    this._updateSelectedPage(e.currentTarget.dataset.index);
  },
  //事件处理函数
  swiperchange: function (e) {
    //console.log('swiperCurrent',e.detail.current)
    let { tabs, stv, activeTab } = this.data;
    activeTab = e.detail.current;
    this.setData({ activeTab: activeTab })
    stv.offset = stv.windowWidth * activeTab;
    this.setData({ stv: this.data.stv })
  },
  toIndexPage: function () {
    wx.switchTab({
      url: "/pages/classification/index"
    });
  },
})