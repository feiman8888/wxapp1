//index.js
//获取应用实例
var app = getApp()
var util = require("../../utils/util.js");
var api = require("../../constant/api.js");
Page({
  data: {
    addressList:[]
  },

  selectTap: function (e) {
    wx.navigateBack({});
  },

  addAddess : function () {
    wx.navigateTo({
      url:"/pages/address-add/index"
    })
  },
  
  editAddess: function (e) {
    wx.navigateTo({
      url: "/pages/address-add/index?id=" + e.currentTarget.dataset.id
    })
  },
  
  onLoad: function () {
    console.log('onLoad')

   
  },
  onShow : function () {
    this.initShippingAddress();
  },
  initShippingAddress: function () {
    var that = this;
    util.request({
      url: '/user/shipping-address/list',
      data: {},
      success: (res) =>{
        if (res.data.code == 0) {
          that.setData({
            addressList:res.data.data
          });
        }
      }
    })
  }

})
