// pages/authorize/index.js
var app = getApp();
var util = require("../../utils/util.js");
var api = require("../../constant/api.js");
Page({

  /**
   * 页面的初始数据
   */
  data: {
  
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },
  rejectLogin: function (e){
    wx.navigateBack({
      
    })
  },
  bindGetUserInfo: function (e) {
    if (!e.detail.userInfo){
      return;
    }
    wx.setStorageSync('userInfo', e.detail.userInfo)
    this.login();
  },
  login: function () {
    let that = this;
    let token = wx.getStorageSync('token');
    if (token) {
      util.request({
        url: '/user/check-token',
        data: {},
        success: function (res) {
          if (res.data.code != 0) {
            wx.removeStorageSync('token')
            that.login();
          } else {
            // 回到原来的地方放
            wx.navigateBack();
          }
        }
      },-1)
      return;
    }
    let code = null;
    util.login().then((res) => {
      code = res.code;
      return util.getUserInfo();
    }).then((userInfo) => {
      //登录远程服务器
      util.request({
        url:'/user/wxapp_login',
        data: { code: code, wxuser: userInfo.encryptedData, iv: userInfo.iv },
        success:function(res){
          if(res.data.code == 0){
            //存储用户信息
            if (res.data.data.userInfo){
              wx.setStorageSync('userInfo', res.data.data.userInfo); //改变本地用户信息
            }
            wx.setStorageSync('uid', res.data.data.uid);
            wx.setStorageSync('token', res.data.data.token);
            // 回到原来的页面
            wx.navigateBack();
          } else {
            // 登录错误
            wx.hideLoading();
            wx.showModal({
              title: '提示',
              content: '无法登录，请重试' + res.data.errmsg,
              showCancel: false
            })
          }
        }
      },-1);
    }).catch((err) => {
      reject(err);
    });
  }
})